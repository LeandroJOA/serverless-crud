const animeService = require('../services/anime-service')

module.exports.handler = async (event) => {

    try {
        const body = JSON.parse(event.body)

        await animeService.create(body)

        return {
            statusCode: 201,
            body: JSON.stringify(body)
        }
    } catch (error) {
        console.error(error);

        return {
            statusCode: 500,
            body: JSON.stringify(`ERROR! Failed to create: ${error}`)
        }
    }
}