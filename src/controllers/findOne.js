const animeService = require('../services/anime-service')

module.exports.handler = async (event) => {

    try {
        const id = event.pathParameters.id

        const anime = await animeService.readOne(id)

        if (anime) {
            return {
                statusCode: 200,
                body: JSON.stringify(anime)
            }
        } else {
            return {
                statusCode: 404,
                body: JSON.stringify({
                    message: 'ERROR! ID not found'
                })
            }
        }
    } catch (error) {
        console.error(error);

        return {
            statusCode: 500,
            body: JSON.stringify(`ERROR! Failed to retrieve: ${error}`)
        }
    }
}